﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using To_Do_List__asp.net_.Entities;
using To_Do_List__asp.net_.Models;

namespace To_Do_List__asp.net_.Controllers
{
    public class HomeController : Controller
    {
        DataBase db = new DataBase();

        public ActionResult Index()
        {
            IEnumerable<Task> task = db.Tasks.ToArray();            
            return View(task);
        }
        
        [HttpGet]
        public ActionResult Remove(int id)
        {
            Task task = db.Tasks.FirstOrDefault(t => t.Id == id);
            db.Tasks.Remove(task);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Task task)
        {
            db.Entry(task).State = System.Data.Entity.EntityState.Added;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
       
    }
}