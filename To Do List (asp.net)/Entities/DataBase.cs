﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Web.Configuration;
using To_Do_List__asp.net_.Models;

namespace To_Do_List__asp.net_.Entities
{
    public class DataBase : DbContext
    {
        public DataBase() : base("To_Do_List__asp.net.DataBase1")
        {
            Database.SetInitializer<DataBase>(new CreateDatabaseIfNotExists<DataBase>());
            Database.SetInitializer<DataBase>(new DropCreateDatabaseIfModelChanges<DataBase>());
        }
        public DbSet<Task> Tasks { get; set; }
    }
}